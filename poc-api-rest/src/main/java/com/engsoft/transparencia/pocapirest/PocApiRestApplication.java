package com.engsoft.transparencia.pocapirest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PocApiRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(PocApiRestApplication.class, args);
	}

}
