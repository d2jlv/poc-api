package com.engsoft.transparencia.pocapirest.repositories;

import com.engsoft.transparencia.pocapirest.entities.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {
    Employee findById(long id);
}
